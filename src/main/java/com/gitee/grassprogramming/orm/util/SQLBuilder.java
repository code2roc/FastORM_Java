package com.gitee.grassprogramming.orm.util;



import com.gitee.grassprogramming.orm.design.BaseEntity;
import com.gitee.grassprogramming.orm.design.OptionType;
import com.mysql.jdbc.StringUtils;

import java.lang.reflect.Field;
import java.text.MessageFormat;
import java.util.List;

/**
 * Created by paul on 2019/1/1.
 */
public class SQLBuilder {
    private MapperUtil mapperUtil;
    public  SQLBuilder() {
        mapperUtil = new MapperUtil();
    }

    public <T extends BaseEntity> String Build(T entity, OptionType optionType) throws Exception {
        String tablename = mapperUtil.GetTableName(entity);
        String KeyFieldName = mapperUtil.GetKeyFieldName(entity.getClass());
        List<Field> fs = mapperUtil.GetMapperFields(entity);
        StringBuilder sql = new StringBuilder();
        switch (optionType) {
            case insert:
                StringBuilder fieldbuilder = new StringBuilder();
                StringBuilder valuebuilder = new StringBuilder();
                sql.append(MessageFormat.format("insert into {0}", tablename));
                fieldbuilder.append("(");
                valuebuilder.append("(");
                try {
                    for (Field f : fs) {

                        if (fs.indexOf(f) == fs.size() - 1) {
                            fieldbuilder.append(f.getName().toString());
                            valuebuilder.append("?");
                        } else {
                            fieldbuilder.append(f.getName().toString() + ",");
                            valuebuilder.append("?,");
                        }


                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                fieldbuilder.append(")");
                valuebuilder.append(")");
                sql.append(" " + fieldbuilder.toString());
                sql.append(" values " + valuebuilder.toString());
                break;
            case delete:
                sql.append(MessageFormat.format("delete from {0} where {1}=?", tablename, KeyFieldName));
                break;
            case update:
                sql.append(MessageFormat.format("update  {0} set ", tablename));
                for (Field f : fs) {
                    if (fs.indexOf(f) == fs.size() - 1) {
                        sql.append(MessageFormat.format(" {0}=?",f.getName()));
                    }else{
                        sql.append(MessageFormat.format(" {0}=?,",f.getName()));
                    }

                }
                sql.append(MessageFormat.format(" where {0}=?",KeyFieldName));
                break;

        }
        return sql.toString();
    }

    public <T extends BaseEntity> String Build(Class<T> tClass,OptionType optionType) throws Exception{
        String tablename = mapperUtil.GetTableName(tClass);
        String KeyFieldName = mapperUtil.GetKeyFieldName(tClass);
        List<Field> fs = mapperUtil.GetMapperFields(tClass);
        StringBuilder sql = new StringBuilder();
        switch (optionType){
            case findList:
                sql.append(MessageFormat.format("select * from {0} where {1}=?",tablename,KeyFieldName));
                break;
            case count:
                sql.append(MessageFormat.format("select count(1)  from {0}",tablename));
                break;
        }
        return sql.toString();
    }

    public <T extends BaseEntity> String Build(Class<T> tClass,String keyName,OptionType optionType) throws Exception{
        String tablename = mapperUtil.GetTableName(tClass);
        List<Field> fs = mapperUtil.GetMapperFields(tClass);
        StringBuilder sql = new StringBuilder();
        switch (optionType){
            case findList:
                sql.append(MessageFormat.format("select * from {0} where {1}=?",tablename,keyName));
                break;
        }
        return sql.toString();
    }

    public <T extends BaseEntity> String Build(Class<T> tClass,OptionType optionType,String columns, String where,String orderBy) throws Exception{
        String tablename = mapperUtil.GetTableName(tClass);
        StringBuilder sql = new StringBuilder();
        switch (optionType){
            case findList:
                sql.append(MessageFormat.format("select {0} from {1} ",columns,tablename));
                if(!StringUtils.isNullOrEmpty(where)){
                    sql.append(" where ");
                    sql.append(where);
                }
                if(!StringUtils.isNullOrEmpty(orderBy)){
                    sql.append(" Order By ");
                    sql.append(orderBy);
                }

                break;
        }
        return sql.toString();
    }

    public <T extends BaseEntity> String Build(Class<T> tClass,OptionType optionType,String columns, String where,String orderBy,int pageIndex,int pageSize) throws Exception{
        String tablename = mapperUtil.GetTableName(tClass);
        StringBuilder sql = new StringBuilder();
        switch (optionType){
            case findPage:
                sql.append(MessageFormat.format("select {0} from {1} ",columns,tablename));
                if(!StringUtils.isNullOrEmpty(where)){
                    sql.append(" where ");
                    sql.append(where);
                }
                if(!StringUtils.isNullOrEmpty(orderBy)){
                    sql.append(" Order By ");
                    sql.append(orderBy);
                }
                sql.append(MessageFormat.format(" limit {0},{1}",pageSize*(pageIndex-1),pageSize));
                break;
        }
        return sql.toString();
    }

    public String Build(String tableName,String columns,String where,String orderBy){
        StringBuilder sql = new StringBuilder();
        sql.append("select ");
        sql.append(columns);
        sql.append(" from ");
        sql.append(tableName);
        if(!StringUtils.isNullOrEmpty(where)){
            sql.append(" where ");
            sql.append(where);
        }
        if(!StringUtils.isNullOrEmpty(orderBy)){
            sql.append(" Order By ");
            sql.append(orderBy);
        }
        return sql.toString();
    }

    public String Build(String tableName,String columns,String where,String orderBy,int pageIndex,int pageSize){
        StringBuilder sql = new StringBuilder();
        sql.append("select ");
        sql.append(columns);
        sql.append(" from ");
        sql.append(tableName);
        if(!StringUtils.isNullOrEmpty(where)){
            sql.append(" where ");
            sql.append(where);
        }
        if(!StringUtils.isNullOrEmpty(orderBy)){
            sql.append(" Order By ");
            sql.append(orderBy);
        }
        sql.append(MessageFormat.format(" limit {0},{1}",pageSize*(pageIndex-1),pageSize));
        return sql.toString();
    }


}
