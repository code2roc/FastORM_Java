package com.gitee.grassprogramming.orm.util;
import com.gitee.grassprogramming.orm.annotation.TableName;
import com.gitee.grassprogramming.orm.base.ClassCacheManage;
import com.gitee.grassprogramming.orm.design.BaseEntity;
import com.gitee.grassprogramming.orm.annotation.Key;
import com.gitee.grassprogramming.orm.design.ClassCache;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by paul on 2018/12/31.
 */
public class MapperUtil {
    public <T> String GetTableName(Class<T> tClass) throws Exception{
        if(ClassCacheManage.getCacheMap().containsKey(tClass)){
            return ClassCacheManage.getCacheMap().get(tClass).getTableName();
        }else{
            throw new RuntimeException("no class cache exception");
        }

    }

    public <T> String GetTableName(T entity) throws Exception{
        if(ClassCacheManage.getCacheMap().containsKey(entity.getClass())){
            return ClassCacheManage.getCacheMap().get(entity.getClass()).getTableName();
        }else{
            throw new RuntimeException("no class cache exception");
        }

    }

    public <T> String GetKeyFieldName(Class<T> tClass){
        if(ClassCacheManage.getCacheMap().containsKey(tClass)){
            return ClassCacheManage.getCacheMap().get(tClass).getKeyFieldName();
        }else{
            throw new RuntimeException("no class cache exception");
        }

    }

    public <T> List<Field> GetMapperFields(T entity){
        if(ClassCacheManage.getCacheMap().containsKey(entity.getClass())){
            return ClassCacheManage.getCacheMap().get(entity.getClass()).getFields();
        }else{
            throw new RuntimeException("no class cache exception");
        }

    }

    public <T> List<Field> GetMapperFields(Class<T> tClass){
        if(ClassCacheManage.getCacheMap().containsKey(tClass)){
            return ClassCacheManage.getCacheMap().get(tClass).getFields();
        }else{
            throw new RuntimeException("no class cache exception");
        }

    }

    public <T> String GetKeyFieldValue(T entity) throws Exception{
        if(ClassCacheManage.getCacheMap().containsKey(entity.getClass())){
            Field keyfield = ClassCacheManage.getCacheMap().get(entity.getClass()).getKeyField();
            return keyfield.get(entity).toString();
        }else{
            throw new RuntimeException("no class cache exception");
        }

    }



}
