package com.gitee.grassprogramming.orm.test;

import com.gitee.grassprogramming.orm.annotation.Key;
import com.gitee.grassprogramming.orm.design.BaseEntity;

/**
 * Created by paul on 2019/1/8.
 */
public class Frame_User extends BaseEntity {
    @Key
    private String loginid;
    private String displayname;
    private String userguid;
    private String password;

    public String getLoginid() {
        return loginid;
    }

    public void setLoginid(String loginid) {
        this.loginid = loginid;
    }

    public String getDisplayname() {
        return displayname;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }

    public String getUserguid() {
        return userguid;
    }

    public void setUserguid(String userguid) {
        this.userguid = userguid;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
