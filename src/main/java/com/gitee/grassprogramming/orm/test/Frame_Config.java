package com.gitee.grassprogramming.orm.test;

import com.gitee.grassprogramming.orm.annotation.Ignore;
import com.gitee.grassprogramming.orm.annotation.Key;
import com.gitee.grassprogramming.orm.annotation.TableName;
import com.gitee.grassprogramming.orm.design.BaseEntity;

import java.util.Date;

/**
 * Created by paul on 2018/12/31.
 */
@TableName(value = "Frame_Config")
public class Frame_Config  extends BaseEntity{
    @Key
    public String ConfigGuid;
    public String ConfigName;
    public String ConfigValue;
    public String ConfiIntro;
    public Date AddDate;
    public String ConfigGroupGuid;
    @Ignore
    public String ExstraStaus;


    public String getConfigGuid() {
        return ConfigGuid;
    }

    public void setConfigGuid(String configGuid) {
        ConfigGuid = configGuid;
    }

    public String getConfigName() {
        return ConfigName;
    }

    public void setConfigName(String configName) {
        ConfigName = configName;
    }

    public String getConfigValue() {
        return ConfigValue;
    }

    public void setConfigValue(String configValue) {
        ConfigValue = configValue;
    }

    public String getConfiIntro() {
        return ConfiIntro;
    }

    public void setConfiIntro(String confiIntro) {
        ConfiIntro = confiIntro;
    }

    public Date getAddDate() {
        return AddDate;
    }

    public void setAddDate(Date addDate) {
        AddDate = addDate;
    }

    public String getConfigGroupGuid() {
        return ConfigGroupGuid;
    }

    public void setConfigGroupGuid(String configGroupGuid) {
        ConfigGroupGuid = configGroupGuid;
    }

    public String getExstraStaus() {
        return ExstraStaus;
    }

    public void setExstraStaus(String exstraStaus) {
        ExstraStaus = exstraStaus;
    }
}
