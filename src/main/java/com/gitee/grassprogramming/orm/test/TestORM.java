package com.gitee.grassprogramming.orm.test;

import com.gitee.grassprogramming.orm.design.BaseEntity;
import com.gitee.grassprogramming.orm.design.BaseExtendEntity;
import com.gitee.grassprogramming.orm.util.DBUtil;
import com.gitee.grassprogramming.orm.util.ScanUtil;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.util.*;
import java.util.logging.Filter;

/**
 * Created by paul on 2019/1/1.
 */
public class TestORM {
    public static void  main(String[] args) throws Exception{
        DBUtil.init(TestORM.class);
        DBUtil dbUtil = new DBUtil();
        //测试查找单个对象
        Frame_Config frame_config = dbUtil.findOne("8c0648f4-c3eb-4447-a0ff-8a63c41ece3b", Frame_Config.class);
        System.out.println(frame_config.ConfigName);
        //测试单个操作指定的库
        DriverManagerDataSource source = dbUtil.getDataSource("jdbc:mysql://127.0.0.1:3306/orm?useUnicode=true&characterEncoding=UTF-8&allowMultiQueries=true","root","Yanpeng24*","com.mysql.jdbc.Driver");
        Frame_User frame_user = dbUtil.findOne("3333", Frame_User.class,source);
        System.out.println(frame_user.getPassword());
        //测试修改
        frame_config.ConfigValue="222";
        dbUtil.update(frame_config);
        //测试修改，传入dto类型的部分包含实体类的参数对象
        Frame_ConfigDto dto = new Frame_ConfigDto();
        dto.setConfigGuid("8c0648f4-c3eb-4447-a0ff-8a63c41ece3b");
        dto.setConfigName("SystemName");
        dto.setConfigValue("12ssaa");
        dto.setIndex(110);
        dto.setKeyWord("test123");
        dbUtil.update(dto,Frame_Config.class);
        //测试SQL日志追踪
        dbUtil.OpenTrace();;
        frame_config = dbUtil.findOne("8c0648f4-c3eb-4447-a0ff-8a63c41ece3b", Frame_Config.class);
        System.out.println(frame_config.ConfigName);
        dbUtil.CloseTrace();
        //测试查找列表
        List<Frame_Config> list = dbUtil.findList("*","",null,"",Frame_Config.class);
        for (Frame_Config f:list) {
            System.out.println(f.getConfigName());
        }
        list = dbUtil.findList("ConfigName","ConfigGuid=?",new Object[]{"8c0648f4-c3eb-4447-a0ff-8a63c41ece3b"},"AddDate desc",Frame_Config.class);
        for (Frame_Config f:list) {
            System.out.println(f.getConfigName());
        }
        //测试分页查找
        list = dbUtil.findPage("*","",null,"AddDate desc",1,1,Frame_Config.class);
        for (Frame_Config f:list) {
            System.out.println(f.getConfigName());
        }
        //测试多表联查
        List<Map<String,Object>> viewlist = dbUtil.findView("frame_user a left join frame_userextend b on a.userguid=b.userguid","b.Email","a.userguid=?",new Object[]{"e7c8f780-ae53-4cd0-b0a7-0a857132f7fe"},"a.userguid desc");
        for (Map<String,Object> item:viewlist) {
            System.out.println(item.get("Email"));
        }
        //测试多表查询分页
        List<Map<String,Object>> viewpagelist = dbUtil.findView("frame_user a left join frame_userextend b on a.userguid=b.userguid","b.Email","a.userguid=?",new Object[]{"e7c8f780-ae53-4cd0-b0a7-0a857132f7fe"},"a.userguid desc",1,1);
        for (Map<String,Object> item:viewpagelist) {
            System.out.println(item.get("Email"));
        }
        //测试获取数量
        int count = dbUtil.getCount(frame_config.getClass());
        System.out.println("count:"+count);
        //测试执行SQL语句
        dbUtil.executeSQL("update frame_userextend set Email=? where userguid=?",new Object[]{"aaaaaaaa","d33ea591-93d4-4f74-bc9e-392db05995c0"});
        //测试直接获取单个字段数据
        String loginid = dbUtil.executeSQLToString("select loginid from frame_user where userguid=?",new Object[]{"e7c8f780-ae53-4cd0-b0a7-0a857132f7fe"});
        System.out.println(loginid);

        //测试增加对象
        frame_config.setConfigGuid(UUID.randomUUID().toString());
        frame_config.setConfigName("test1111111");
        frame_config.setConfigValue("2222222");
        frame_config.setConfiIntro("hahahahhah");
        frame_config.setAddDate(new Date());
        dbUtil.insert(frame_config);
        //测试删除对象
        dbUtil.delete(frame_config);
        //测试事物
        /*
        DriverManagerDataSource transitionDataSource = dbUtil.getTransitionDataSource();
        dbUtil.executeTransition(transitionDataSource, new TransitionAction() {
            @Override
            public void doTransitionOption(DriverManagerDataSource dataSource){
               try {
                   Frame_Config frame_config = new Frame_Config();
                   frame_config.setConfigGuid(UUID.randomUUID().toString());
                   frame_config.setConfigName("test1111111");
                   frame_config.setConfigValue("2222222");
                   frame_config.setConfiIntro("hahahahhah");
                   frame_config.setAddDate(new Date());
                   dbUtil.insert(frame_config,dataSource);
                   throw new RuntimeException();
               }
               catch (Exception e){
                   e.printStackTrace();
                   throw new RuntimeException();
               }
            }
        });
        */
        //批量插入
        /*
        List<Frame_Config> batchlist = new ArrayList<>();
        for (int i=0;i<10;i++){
            Frame_Config f = new Frame_Config();
            f.setConfigGuid(UUID.randomUUID().toString());
            f.setConfigName("test1111111");
            f.setConfigValue("2222222");
            f.setConfiIntro("hahahahhah");
            f.setAddDate(new Date());
            batchlist.add(f);
        }
        dbUtil.insertBatch(batchlist);
        */

    }
}
