package com.gitee.grassprogramming.orm.test;

import com.gitee.grassprogramming.orm.design.BaseExtendEntity;

/**
 * Created by paul on 2019/1/4.
 */
public class Frame_ConfigDto extends BaseExtendEntity {
    public String ConfigGuid;
    public String ConfigName;
    public String ConfigValue;
    public int Index;
    public String KeyWord;

    public String getConfigGuid() {
        return ConfigGuid;
    }

    public void setConfigGuid(String configGuid) {
        ConfigGuid = configGuid;
    }

    public String getConfigName() {
        return ConfigName;
    }

    public void setConfigName(String configName) {
        ConfigName = configName;
    }

    public String getConfigValue() {
        return ConfigValue;
    }

    public void setConfigValue(String configValue) {
        ConfigValue = configValue;
    }

    public int getIndex() {
        return Index;
    }

    public void setIndex(int index) {
        Index = index;
    }

    public String getKeyWord() {
        return KeyWord;
    }

    public void setKeyWord(String keyWord) {
        KeyWord = keyWord;
    }
}
