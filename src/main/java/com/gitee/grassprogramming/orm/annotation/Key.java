package com.gitee.grassprogramming.orm.annotation;

import java.lang.annotation.*;

/**
 * Created by paul on 2018/12/31.
 */
@Documented
@Inherited
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface  Key {
}
