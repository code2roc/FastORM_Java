package com.gitee.grassprogramming.orm.annotation;

import java.lang.annotation.*;

/**
 * Created by paul on 2019/1/8.
 */
@Documented
@Inherited
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Ignore {
}
