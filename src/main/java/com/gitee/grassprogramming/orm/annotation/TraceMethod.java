package com.gitee.grassprogramming.orm.annotation;

import java.lang.annotation.*;

/**
 * Created by paul on 2019/1/3.
 */
@Documented
@Inherited
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface TraceMethod {
}
