package com.gitee.grassprogramming.orm.design;

import java.lang.reflect.Field;
import java.util.List;

/**
 * Created by paul on 2019/1/7.
 */
public class ClassCache {
    private String tableName;
    private String keyFieldName;
    private List<Field> fields;
    private Field keyField;

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getKeyFieldName() {
        return keyFieldName;
    }

    public void setKeyFieldName(String keyFieldName) {
        this.keyFieldName = keyFieldName;
    }

    public List<Field> getFields() {
        return fields;
    }

    public void setFields(List<Field> fields) {
        this.fields = fields;
    }

    public Field getKeyField() {
        return keyField;
    }

    public void setKeyField(Field keyField) {
        this.keyField = keyField;
    }
}
