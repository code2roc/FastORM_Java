package com.gitee.grassprogramming.orm.design;

/**
 * Created by paul on 2019/1/1.
 */
public enum OptionType {
    insert,
    delete,
    update,
    findList,
    findPage,
    count
}
