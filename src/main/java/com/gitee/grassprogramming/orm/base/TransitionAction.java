package com.gitee.grassprogramming.orm.base;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 * Created by paul on 2019/1/3.
 */
@FunctionalInterface
public interface TransitionAction {
    void doTransitionOption(DriverManagerDataSource dataSource);
}
