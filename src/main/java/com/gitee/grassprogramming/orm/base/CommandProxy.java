package com.gitee.grassprogramming.orm.base;


import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;
import com.gitee.grassprogramming.orm.annotation.TraceMethod;
import com.gitee.grassprogramming.orm.util.LogUtil;
import java.lang.reflect.Method;

/**
 * Created by paul on 2019/1/3.
 */
public class CommandProxy implements MethodInterceptor {
    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        //追踪指定的方法
        if(method.isAnnotationPresent(TraceMethod.class)){
            Command command = (Command)o;
            //开启sql跟踪，打印sql语句及参数
            if(command.isTraceSQL()){
                LogUtil.writeLog("=========================================================");
                LogUtil.writeLog("SQL语句:"+command.getSQLText());
            }
        }
        return methodProxy.invokeSuper(o, objects);
    }
}
