package com.gitee.grassprogramming.orm.base;

import com.gitee.grassprogramming.orm.annotation.Ignore;
import com.gitee.grassprogramming.orm.annotation.Key;
import com.gitee.grassprogramming.orm.annotation.TableName;
import com.gitee.grassprogramming.orm.design.BaseEntity;
import com.gitee.grassprogramming.orm.design.BaseExtendEntity;
import com.gitee.grassprogramming.orm.design.ClassCache;
import com.gitee.grassprogramming.orm.test.TestORM;
import com.gitee.grassprogramming.orm.util.MapperUtil;
import com.gitee.grassprogramming.orm.util.ScanUtil;

import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by paul on 2019/1/7.
 */
public class ClassCacheManage {
    private static Map<Class,ClassCache> cacheMap;
    public static  void initClassCache(Class runClass) throws  Exception{
        if(null==cacheMap){
            cacheMap = new HashMap<Class,ClassCache>();
        }else{
            cacheMap.clear();
        }

        List<Class> classfilter = new ArrayList<Class>();
        classfilter.add(BaseEntity.class);
        classfilter.add(BaseExtendEntity.class);
        List<Class<?>> clsList = ScanUtil.getAllClassByPackageName(runClass.getPackage(),classfilter);
        //进行所有class类的缓存
        for(Class item:clsList){
            ClassCache classCache = new ClassCache();
            String tableName = InitTableName(item);
            Field keyField = initKeyField(item);
            String keyFieldName = "";
            if(null!=keyField){
                keyFieldName = keyField.getName();
            }
            List<Field> fs = Arrays.asList(item.getDeclaredFields());
            //去除标记Ignore的字段不映射
            fs = fs.stream().filter(((Field f)-> !f.isAnnotationPresent(Ignore.class))).collect(Collectors.toList());
            classCache.setTableName(tableName);
            classCache.setKeyField(keyField);
            classCache.setKeyFieldName(keyFieldName);
            classCache.setFields(fs);
            cacheMap.put(item,classCache);
        }
        System.out.println("初始化ORM缓存成功，共缓存"+cacheMap.size()+"个class");
    }

    private static <T> String InitTableName(Class<T> entityClass) throws Exception{
        String name = entityClass.getName().replace(entityClass.getPackage().getName()+".","");
        TableName annotation =  entityClass.getAnnotation(TableName.class);
        if(null!=annotation){
            name =  annotation.value();
        }
        return name;
    }

    private static  <T> Field initKeyField(Class<T> tClass){
        Field keyField = null;
        Field[] fs = tClass.getDeclaredFields();
        for (Field f:fs) {
            if(f.isAnnotationPresent(Key.class)){
                keyField = f;
                break;
            }
        }
        return keyField;
    }

    public static Map<Class, ClassCache> getCacheMap() {
        return cacheMap;
    }

    public static void setCacheMap(Map<Class, ClassCache> cacheMap) {
        ClassCacheManage.cacheMap = cacheMap;
    }
}
