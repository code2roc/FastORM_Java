### 基于jdbctemplate和transitiontemplate封装的适用于springboot的轻量级orm框架（mysql数据库）  
博客介绍：https://blog.csdn.net/u013407099/article/details/86551895  

maven引用：

```
<dependency>
  <groupId>com.gitee.grassprogramming</groupId>
  <artifactId>fastorm</artifactId>
  <version>0.0.2</version>
</dependency>
```
 **使用示例：** 
 **声明实体类** 

```
@TableName(value = "Frame_Config")
public class Frame_Config  extends BaseEntity{
    @Key
    public String ConfigGuid;
    public String ConfigName;
    public String ConfigValue;
    public String ConfiIntro;
    public Date AddDate;
    public String ConfigGroupGuid;
    @Ignore
    public String ExstraStaus;


    public String getConfigGuid() {
        return ConfigGuid;
    }

    public void setConfigGuid(String configGuid) {
        ConfigGuid = configGuid;
    }

    public String getConfigName() {
        return ConfigName;
    }

    public void setConfigName(String configName) {
        ConfigName = configName;
    }

    public String getConfigValue() {
        return ConfigValue;
    }

    public void setConfigValue(String configValue) {
        ConfigValue = configValue;
    }

    public String getConfiIntro() {
        return ConfiIntro;
    }

    public void setConfiIntro(String confiIntro) {
        ConfiIntro = confiIntro;
    }

    public Date getAddDate() {
        return AddDate;
    }

    public void setAddDate(Date addDate) {
        AddDate = addDate;
    }

    public String getConfigGroupGuid() {
        return ConfigGroupGuid;
    }

    public void setConfigGroupGuid(String configGroupGuid) {
        ConfigGroupGuid = configGroupGuid;
    }

    public String getExstraStaus() {
        return ExstraStaus;
    }

    public void setExstraStaus(String exstraStaus) {
        ExstraStaus = exstraStaus;
    }
}
```
注：  
1.TableName注解中的值就是表名，如果实体类没有TablName注解，默认类名为表名  
2.Key注解表明实体类的主键，数据库映射实体必须包含主键  
3.Ignore注解表明实体类中不需要映射的字段，例如程序中使用的额外状态位等

 **初始化缓存** 

```
DBUtil.init(TestORM.class);
```
注：fastorm会调用init方法将扫描继承自BaseEntity（数据库实体映射类），BaseExtendEntity（与数据库实体类有交集的类，如dto等）这两个类的子类，进行内存缓存，优化反射效率，该方法请在应用启动时调用，例如：

```
@SpringBootApplication
public class Application {

	public static void main(String[] args) throws  Exception{
		SpringApplication.run(Application.class, args);
		DBUtil.init(Application.class);
    }
}
```
 **测试查找单个对象** 

```
 Frame_Config frame_config = dbUtil.findOne("8c0648f4-c3eb-4447-a0ff-8a63c41ece3b", Frame_Config.class);
```
 **测试单个操作指定的库** 

```
DriverManagerDataSource source = dbUtil.getDataSource("jdbc:mysql://127.0.0.1:3306/orm?useUnicode=true&characterEncoding=UTF-8&allowMultiQueries=true","root","11111","com.mysql.jdbc.Driver");
Frame_User frame_user = dbUtil.findOne("3333", Frame_User.class,source);
```
 **测试修改** 

```
frame_config.ConfigValue="222";
dbUtil.update(frame_config);
```
注：这里的修改是实体类全字段更新  

 **测试修改，传入dto类型的部分包含实体类的参数对象** 

```
Frame_ConfigDto dto = new Frame_ConfigDto();
dto.setConfigGuid("8c0648f4-c3eb-4447-a0ff-8a63c41ece3b");
dto.setConfigName("SystemName");
dto.setConfigValue("12ssaa");
dto.setIndex(110);
dto.setKeyWord("test123");
dbUtil.update(dto,Frame_Config.class);
```
注：实体类扩展类实现更新，扩展类一定要继承自BaseExtendEntity,其中需要包含和数据库实体映射类同名主键字段，会更新扩展类中与实体类中同名字段的值  
 **测试SQL日志追踪** 

```
dbUtil.OpenTrace();;
frame_config = dbUtil.findOne("8c0648f4-c3eb-4447-a0ff-8a63c41ece3b", Frame_Config.class);
System.out.println(frame_config.ConfigName);
dbUtil.CloseTrace();
```
注：支持打开sql跟踪，默认为关闭后，打开后全局sql都会打印在控制台，需要手动关闭  
 **测试查找列表** 

```
List<Frame_Config> list = dbUtil.findList("*","",null,"",Frame_Config.class);
list = dbUtil.findList("ConfigName","ConfigGuid=?",new Object[]{"8c0648f4-c3eb-4447-a0ff-8a63c41ece3b"},"AddDate desc",Frame_Config.class);
```
 **测试分页查找** 

```
list = dbUtil.findPage("*","",null,"AddDate desc",1,1,Frame_Config.class);
```
 **测试多表联查** 

```
List<Map<String,Object>> viewlist = dbUtil.findView("frame_user a left join frame_userextend b on a.userguid=b.userguid","b.Email","a.userguid=?",new Object[]{"e7c8f780-ae53-4cd0-b0a7-0a857132f7fe"},"a.userguid desc");  

List<Map<String,Object>> viewpagelist = dbUtil.findView("frame_user a left join frame_userextend b on a.userguid=b.userguid","b.Email","a.userguid=?",new Object[]{"e7c8f780-ae53-4cd0-b0a7-0a857132f7fe"},"a.userguid desc",1,1);
```
 **测试获取数量** 

```
int count = dbUtil.getCount(frame_config.getClass());
```
 **测试执行SQL语句** 

```
dbUtil.executeSQL("update frame_userextend set Email=? where userguid=?",new Object[]{"aaaaaaaa","d33ea591-93d4-4f74-bc9e-392db05995c0"});
```
 **测试直接获取单个字段数据** 

```
String loginid = dbUtil.executeSQLToString("select loginid from frame_user where userguid=?",new Object[]{"e7c8f780-ae53-4cd0-b0a7-0a857132f7fe"});
```
 **测试增加对象** 

```
frame_config.setConfigGuid(UUID.randomUUID().toString());
frame_config.setConfigName("test1111111");
frame_config.setConfigValue("2222222");
frame_config.setConfiIntro("hahahahhah");
frame_config.setAddDate(new Date());
dbUtil.insert(frame_config);
```
 **测试删除对象** 

```
dbUtil.delete(frame_config);
```
 **测试事物** 

```
 DriverManagerDataSource transitionDataSource = dbUtil.getDataSource();
        dbUtil.executeTransition(transitionDataSource, new TransitionAction() {
            @Override
            public void doTransitionOption(DriverManagerDataSource dataSource){
               try {
                   Frame_Config frame_config = new Frame_Config();
                   frame_config.setConfigGuid(UUID.randomUUID().toString());
                   frame_config.setConfigName("test1111111");
                   frame_config.setConfigValue("2222222");
                   frame_config.setConfiIntro("hahahahhah");
                   frame_config.setAddDate(new Date());
                   dbUtil.insert(frame_config,dataSource);
                   throw new RuntimeException();
               }
               catch (Exception e){
                   e.printStackTrace();
                   throw new RuntimeException();
               }
            }
        });
```
注：事物的执行方法中参数TransitionAction的重载方法中，放入所有需要进行事物整合的操作，其中的操作需要datasource是同一个,可以使用工具提供的getDataSource方法，每个方法都提供了指定datasource的重载函数

 **批量插入** 

```
List<Frame_Config> batchlist = new ArrayList<>();
        for (int i=0;i<10;i++){
            Frame_Config f = new Frame_Config();
            f.setConfigGuid(UUID.randomUUID().toString());
            f.setConfigName("test1111111");
            f.setConfigValue("2222222");
            f.setConfiIntro("hahahahhah");
            f.setAddDate(new Date());
            batchlist.add(f);
        }
        dbUtil.insertBatch(batchlist);
```
注：批量插入使用事物实现，和事物不要混用